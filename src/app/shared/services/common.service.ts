import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CommonModel } from '../model/common-model';

export abstract class CommonService<T extends CommonModel> {
  protected path!: string;
  private map: Map<number, T> = new Map<number, T>();
  // inject httpclient here
  protected http!: HttpClient;

  values(): T[] {
    return Array.from(this.map.values());
  }

  // get all
  getAll(
    params: { [key: string]: any } = {},
    pathExtra: string[] = []
  ): Observable<T[]> {
    return new Observable<T[]>((observer) => {
      const options: HttpParams = new HttpParams();
      options.appendAll(params);
      const pathToUse = `${this.path}${
        pathExtra ? '/' + pathExtra.join('/') : ''
      }`;
      this.http
        .get(pathToUse, {
          params: options || {},
        })
        .subscribe({
          next: (response: any) => {
            const dataResponse: any[] = response.data;
            dataResponse.forEach((item) => {
              const itemParsed = this.fromRaw(item);
              this.map.set(item.id, itemParsed);
            });
            observer.next(dataResponse);
          },
          complete: () => {
            observer.complete();
          },
          error: (err) => {
            observer.error(err);
          },
        });
    });
  }
  fromRaw(item: any): T {
    return {} as T;
  }

  toRaw(item: T): any {
    return item as any;
  }

  // get by id
  getById(id: number): Observable<T> {
    return new Observable<T>((observer) => {
      if (this.map.has(id)) {
        observer.next(this.map.get(id));
        observer.complete();
        return;
      }

      this.http.get(`${this.path}/${id}`).subscribe({
        next: (response: any) => {
          if (!response) {
            observer.next();
            return;
          }
          const dataResponse: T = this.fromRaw(response.data);
          this.map.set(dataResponse.id, dataResponse);
          observer.next(dataResponse);
        },
        complete: () => {
          observer.complete();
        },
        error: (err) => {
          observer.error(err);
        },
      });
    });
  }

  // create
  create(data: T): Observable<T> {
    return new Observable<T>((observer) => {
      const dataToSave = this.toRaw(data);
      this.http.post(this.path, dataToSave).subscribe({
        next: (response: any) => {
          const dataResponse: T = this.fromRaw(response.data);
          this.map.set(dataResponse.id, dataResponse);
          observer.next(dataResponse);
        },
        complete: () => {
          observer.complete();
        },
        error: (err) => {
          observer.error(err);
        },
      });
    });
  }
}
