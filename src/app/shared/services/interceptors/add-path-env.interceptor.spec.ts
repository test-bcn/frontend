import { TestBed } from '@angular/core/testing';

import { AddPathEnvInterceptor } from './add-path-env.interceptor';

describe('AddPathEnvInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      AddPathEnvInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: AddPathEnvInterceptor = TestBed.inject(AddPathEnvInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
