import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AddPathEnvInterceptor } from './shared/services/interceptors/add-path-env.interceptor';
import { CommentsComponent } from './modules/flights/views/comments/comments.component';
import { WelcomeComponent } from './shared/components/welcome/welcome.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [AppComponent, CommentsComponent, WelcomeComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    RouterModule,
  ],
  providers: [
    // add interceptor
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AddPathEnvInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
