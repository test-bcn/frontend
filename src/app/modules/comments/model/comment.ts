import { CommonModel } from 'src/app/shared/model/common-model';

export interface Comment extends CommonModel {
  userId: number;
  flightId: number;
  content: string;
  tags: string[];
}
