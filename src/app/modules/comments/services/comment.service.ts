import { Injectable, inject } from '@angular/core';
import { CommonService } from 'src/app/shared/services/common.service';
import { Comment } from '../model/comment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class CommentService extends CommonService<Comment> {
  constructor() {
    super();
    this.path = 'comments';
    this.http = inject(HttpClient);
  }

  override fromRaw(data: any): Comment {
    const {
      id,
      content,
      user_id: userId,
      flight_id: flightId,
      created_at: createdAt,
      tags,
    } = data;
    return { id, content, userId, flightId, tags, createdAt };
  }

  override toRaw(data: Comment): any {
    const {
      id,
      content,
      userId: user_id,
      flightId: flight_id,
      tags,
      createdAt: created_at,
    } = data;
    return { id, content, user_id, flight_id, tags, created_at };
  }
}
