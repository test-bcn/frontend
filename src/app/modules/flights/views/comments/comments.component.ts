import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommentService } from 'src/app/modules/comments/services/comment.service';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.sass'],
})
export class CommentsComponent implements OnInit {
  flightId = 0;
  constructor(
    private router: ActivatedRoute,
    private commentService: CommentService
  ) {}

  ngOnInit(): void {
    this.router.params.subscribe(({ id }) => {
      this.flightId = parseInt(id, 10);
      this.initData();
    });
  }

  initData(): void {
    this.commentService
      .getAll({}, ['flight', this.flightId.toString()])
      .subscribe({
        next: (comments) => {
          console.log(comments);
        },
        complete: () => {},
        error: (err) => {
          console.log(err);
        },
      });
  }
}
