import { Injectable, inject } from '@angular/core';
import { CommonService } from 'src/app/shared/services/common.service';
import { Flight } from '../model/flight';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class FlightService extends CommonService<Flight> {
  constructor() {
    super();
    this.path = 'flights';
    this.http = inject(HttpClient);
  }

  override fromRaw(data: any): Flight {
    const { id, title, created_at: createdAt } = data;
    return { id, title, createdAt };
  }
}
