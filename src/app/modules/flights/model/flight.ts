import { CommonModel } from 'src/app/shared/model/common-model';

export interface Flight extends CommonModel {
  title: string;
}
