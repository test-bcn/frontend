import { Component, OnInit } from '@angular/core';
import { Flight } from './modules/flights/model/flight';
import { FlightService } from './modules/flights/services/flight.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
})
export class AppComponent implements OnInit {
  title = 'app-test-comments';
  flights: Flight[] = [];

  constructor(private flightService: FlightService) {}
  ngOnInit(): void {
    this.initData();
  }

  initData(): void {
    this.flightService.getAll().subscribe((data) => {
      this.flights = data;
    });
  }
}
